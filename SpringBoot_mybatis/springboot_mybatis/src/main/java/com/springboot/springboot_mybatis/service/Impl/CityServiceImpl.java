package com.springboot.springboot_mybatis.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.springboot_mybatis.bean.City;
import com.springboot.springboot_mybatis.dao.CityDao;
import com.springboot.springboot_mybatis.service.CityService;

@Service
public class CityServiceImpl implements CityService{
	@Autowired
	private CityDao cityDao;

	public City findByName(String cityname) {
		return cityDao.findByName(cityname);
	}

}
