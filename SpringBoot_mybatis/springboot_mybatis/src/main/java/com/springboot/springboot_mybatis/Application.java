package com.springboot.springboot_mybatis;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;

@SpringBootApplication
@MapperScan("com.springboot.springboot_mybatis.dao")
public class Application extends SpringBootServletInitializer{ 
	@Override  
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) 
	{     
		return builder.sources(Application.class);   
    } 
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class,args);
	}
	
}
