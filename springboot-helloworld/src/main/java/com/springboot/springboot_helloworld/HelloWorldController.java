package com.springboot.springboot_helloworld;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 * spring boot helloworld栗子
 */


@RestController
@RequestMapping("/HelloWorld")
public class HelloWorldController {
	@RequestMapping("/sayHello")
	public String sayHello() {
		return "HelloWorld!";
	}

}
